const gulp = require('gulp')
const gulpIf = require('gulp-if')
const flatten = require('gulp-flatten')
const imagemin = require('gulp-imagemin')
const stylus = require('gulp-stylus')
const sourcemaps = require('gulp-sourcemaps')
const csso = require('csso-stylus')
const autoprefixer = require('autoprefixer-stylus')
const { rollup } = require('rollup')
const babel = require('rollup-plugin-babel')
const nodeResolve = require('rollup-plugin-node-resolve')
const commonjs = require('rollup-plugin-commonjs')
const replace = require('rollup-plugin-replace')
const globals = require('rollup-plugin-node-globals')
const builtins = require('rollup-plugin-node-builtins')
const browserSync = require('browser-sync').create()
const rimraf = require('rimraf')

gulp.task('js', () => (
	rollup({
		entry: './src/index.js',
		plugins: [
			babel({
				babelrc: false,
				presets: [ [ 'es2015', { modules: false } ], 'stage-0', 'react' ],
				plugins: [ 'external-helpers' ],
				exclude: 'node_modules/**',
			}),
			commonjs(),
			globals(),
			builtins(),
			replace({
				'process.env.NODE_ENV': JSON.stringify('development')
			}),
			nodeResolve({
				main: true,
				browser: true,
			}),
		]
	}).then(bundle => bundle.write({
		dest: './dist/s/index.js',
		format: 'iife',
		sourceMap: 'inline'
	}))
))

gulp.task('css', () => (
	gulp.src('./src/index.styl')
		.pipe(sourcemaps.init())
		.pipe(stylus({
			url: 'embedurl',
			use: [
				autoprefixer(),
				csso,
			],
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./dist/s'))
))

gulp.task('assets', () => (
	gulp.src('./src/**/*.+(gif|png|jpg|jpeg|woff|woff2|eot|ttf|svg)')
		.pipe(gulpIf('*.+(gif|png|jpg|jpeg|svg)', imagemin()))
		.pipe(flatten())
		.pipe(gulp.dest('./dist/s/assets'))
))

gulp.task('clear', cb => rimraf('./dist', cb))

gulp.task('serve', () => {
	browserSync.init({
		server: {
			baseDir: './',
			middleware: [
				function (req, res, next) {
					const fetch = require('fetch')
					const url = require('url')
					const { pathname } = url.parse(req.url)
					const [ noop, prefix, id, suffix  ] = pathname.split('/');

					if (prefix === 'bins' && id && suffix === 'from') {
						fetch.fetchUrl(`https://rocketbank.ru${pathname}`, (err, meta, body) => {
							res.end(body.toString())
						})
					} else {
						next()
					}
				}
			]
		}
	})

	gulp.watch('./dist/**/*.*').on('change', browserSync.reload)
})

gulp.task('watch', () => {
	const watch = {
		css: {
			files: [ './src/*.styl', './src/**/*.styl' ],
			tasks: 'css',
		},
		js: {
			files: [ './src/*.js', './src/**/*.js' ],
			tasks: 'js',
		},
		assets: {
			files: './src/**/*.+(gif|png|jpg|jpeg|woff|woff2|eot|ttf|svg)',
			tasks: 'assets'
		}
	}

	Object.keys(watch).forEach(name => {
		const { files, tasks } = watch[name]

		gulp.watch(files, {}, gulp.series(tasks))
			.on('error', function() {
				this.emit('end')
			})
	})
})


gulp.task('default', gulp.series('clear', gulp.parallel('js', 'css', 'assets'), gulp.parallel('watch', 'serve')))