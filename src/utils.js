const bind = (fn, ctx) => fn.apply(ctx)

export {
	bind
}