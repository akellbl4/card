import React from 'react';
import { mix } from 'es6-mixin';
import PaySystem from '../PaySystem';
import Loader from '../Loader';

const defaultFormat = /(\d{1,4})/g

const imagePromise = src => {
	const promise = new Promise((resolve, reject) => {
		const image = new Image();

		image.src = src;
		image.onload = () =>{
			resolve();
		}
	});

	return promise;
}

const cards = [{
	type: 'maestro',
	pattern: /^(5018|5020|5038|6304|6703|6708|6759|676[1-3])/,
},
{
	type: 'mastercard',
	pattern: /^5[1-5]/,
},
{
	type: 'visa',
	pattern: /^4/,
},
{
	type: 'visaelectron',
	pattern: /^4(026|17500|405|508|844|91[37])/,
}]

const cardFromNumber = (num) => {
	num = (num + '').replace(/\D/g, '');

	for (var i = 0, l = cards.length; i < l; i++) {
		let card = cards[i];

		if (card.pattern.test(num)) {
			return card;
		}
	}
}

const hasTextSelected = target => {
	try {
		if ((target.selectionStart != null) && target.selectionStart !== target.selectionEnd) {
			return true;
		}
		const selection = document.selection;

		if (selection && selection.createRange && selection.createRange().text) {
				return true;
		}
	} catch (error) {
		return false;
	}
}

export default class Card extends React.Component {
	constructor() {
		super();

		this.state = {
			enter: true,
			flipped: false,
			leave: false,
			first4digits: '0000',
		};

	}

	componentDidMount() {
		this.refs.numberInput.focus();

		setTimeout(() => {
			this.setState({
				enter: false
			})
		}, 1000)
	}

	onNextClick() {
		if (this.state.flipped) {
			this.setState({
				leave: true
			})
		} else {
			this.toggleCard();
			this.refs.cvvInput.focus();
		}
	}

	toggleCard() {
		this.setState(prevState => ({
			flipped: !prevState.flipped
		}));
	}

	onNumberKeypress(evt) {
		this.restrictNumeric(evt);
		this.restrictCardNumber(evt);
		this.formatCardNumber(evt);
	}

	onNumberKeydown(evt) {
		const { target, meta, which } = evt;
		const value = target.value;


		if (meta || which !== 8 || hasTextSelected(target)) {
			return false;
		}

		if (/\d\s\s$/.test(value)) {
			evt.preventDefault();
			target.value = value.replace(/\d\s\s$/, '');
		} else if (/\s\s\d?$/.test(value)) {
			evt.preventDefault();
			target.value = value.replace(/\s\s\d?$/, '');
		}
	}

	onNumberKeyup(evt) {
		const { target } = evt;
		const { value } = target;
		const card = cardFromNumber(value)
		let type = ''

		this.fillFirst4digits(value);
		this.getBankName(value);

		if (card) {
			type = card.type;
		}

		this.setState({ type });
	}

	onNumberPaste(evt) {
		setTimeout(() => {
			const { target } = evt;

			target.value = formatCardNumber(target);
			this.fillFirst4digits(target.value);
			this.getBankName(value);
		}, 0)
	}

	restrictCardNumber(evt) {
		const { target } = evt;
		const digit = String.fromCharCode(evt.which);

		if (isNaN(digit) || hasTextSelected(target)) {
			return false;
		}

		const value = (target.value + digit).replace(/\D/g, '');

		if (value.length > 16) {
			evt.preventDefault();
		}
	}

	formatCardNumber (evt) {
		const { target } = evt;
		const digit = String.fromCharCode(evt.which);

		if (isNaN(digit) || hasTextSelected(target)) {
			return false;
		}
		const { value } = target;
		const length = (value.replace(/\D/g, '') + digit).length;

		if (length >= 16) {
			this.validateFields();
			this.refs.dateInput.focus();
			return false;
		}

		const re = /(?:^|\s\s)(\d{4})$/;

		if (re.test(value)) {
			evt.preventDefault();
			target.value += `  ${digit}`;
		} else if (re.test(value + digit)) {
			evt.preventDefault();
			target.value += `${digit}  `;
		}
	}

	restrictNumeric(evt) {
		const { metaKey, ctrlKey, which } =  evt;

		if (metaKey || ctrlKey) {
			return true;
		}

		if (which === 32) {
			return evt.preventDefault();
		} else if (which === 0 || which < 33) {
			return true;
		}

		const input = String.fromCharCode(evt.which);

		if (!/[\d\s\s]/.test(input)) {
			return evt.preventDefault();
		}
	}

	onDateKeydown(evt) {
		const { target, metaKey, which } = evt;
		const symbol = String.fromCharCode(which);
		const value = target.value + symbol;

		if (value.length === 6) {
			this.validateFields();
			this.refs.nameInput.focus();
		}

		if (hasTextSelected(target) || metaKey) {
			return false;
		} else if (!isNaN(symbol)) { /* digits or select */
			if (/^\d$/.test(value) && (value !== '0' && value !== '1')) { /* one digit, digit not 0 or 1 */
				evt.preventDefault();
				target.value = `0${value}/`;
			} else if (/^\d\d$/.test(value)) { /* two digit */
				evt.preventDefault();
				target.value = `${value}/`;
			} else if (/^\d\d\/\d\d\d$/.test(value)) {
				evt.preventDefault();
			}
		} 
	}

	onDateKeypress(evt) {
		const { which, target } = evt;
		const symbol = String.fromCharCode(which);
		const value = target.value;

		if (symbol === '/' && /^\d$/.test(value) && value !== '0') { /* one digit and not 0 */
			evt.preventDefault();
			target.value = `0${value}/`;
		} 
	}

	onDateKeyup(evt) {
		const { target, metaKey, which } = evt;
		const value = target.value.replace(/\D/g, '');

		if (metaKey || hasTextSelected(target)) {
			return false;
		} else if (which == 8) {
			if (/\d\/$/.test(value)) {
				evt.preventDefault();
				target.value = value.replace(/\d\/*$/, '');
			} else if (/\/?\d?$/.test(value)) {
				evt.preventDefault();
				target.value = value.replace(/\/?\d?$/, '');
			}
		}
	}

	onNameType() {
		this.validateFields();
	}

	onCvvKeydow(evt) {
		const { target, which } = evt;
		const digit = String.fromCharCode(which);
		
		if (!isNaN(digit)) {
			const { value } = target;

			evt.preventDefault();

			if (value.length + 1 < 4) {
				target.value += digit;
				if (target.value.length === 3) {
					this.setState({
						leave: true
					})
				}
			}
		}
	}

	validateFields() {
		const { nameInput, dateInput, numberInput } = this.refs;
		const valid = numberInput.value.length > 21 && dateInput.value.length === 5 && nameInput.value.length > 5;

		this.setState({
			valid
		})

		return valid;
	}

	fillFirst4digits(value) {
		this.setState(({ first4digits }) => {
			const length = value.length;

			if (length < 4) {
				value += '0000'.substr(length, 4)
			}
			return {
				first4digits: value.substr(0, 4)
			}
		})
	}

	getBankName(value) {
		let fetching;
		let fetchTimeout;

		return (() => {
			if (this.state.loading) {
				return false;
			}

			clearTimeout(fetchTimeout)
			fetchTimeout = setTimeout(() => {
				value = value.replace(/\s+/g, '');

				if (value.length === 6) {
					value = value.substr(0, 6);
					this.setState({
						loading: true
					});

					fetch(`/bins/${value}/from`)
						.then(res => {
							if (res.status === 200) {
								return res.json()
							}
						})
						.then(({ image, logo }) => {
							if (logo && image) {
								Promise.all([
									imagePromise(image),
									imagePromise(logo)
								]).then(res => this.setState({
									logo,
									image,
									loading: false
								}));
							} else {
								this.setState({
									loading: false
								})
							}
						})
						.catch(err => console.log(err))
				} else if (value.length === 5) {
					this.setState({
						logo: '',
						image: ''
					})
				}
			}, 300)

		})()
	}

	render() {
		const { enter, flipped, leave, type, first4digits, logo, image, loading, valid } = this.state;
		const banklogoStyle = {};
		const cardStyle = {};
		const cardClassName = [ 'card' ];
		const numberInputClassName = [ 'card-input card-input_number' ]

		if (enter) {
			cardClassName.push('card_enter');
		}
		if (flipped) {
			cardClassName.push('card_fliped');
		}
		if (leave) {
			cardClassName.push('card_leave');
		}

		if (logo && image) {
			banklogoStyle.backgroundImage = `url(${logo})`;
			cardStyle.backgroundImage = `url(${image})`;
			cardClassName.push('card_withbg');
		}

		return (
			<div className={cardClassName.join(' ')}>
				<div className="card-block">
					<div className="card-front" style={cardStyle}>
						{logo ? <div className="card-banklogo" style={banklogoStyle}></div> : ''}
						{loading ? <Loader className="card-loader"/> : ''}
						{valid ? <div className="card-flip" onClick={this.onNextClick.bind(this)}/> : ''}
						<label className="card-number">
							<input
									className={numberInputClassName.join(' ')}
									ref="numberInput"
									onPaste={this.onNumberPaste.bind(this)}
									onKeyPress={this.onNumberKeypress.bind(this)}
									onKeyUp={this.onNumberKeyup.bind(this)}
									onKeyDown={this.onNumberKeydown}
									type="text"
									tabIndex="1"
									autoComplete="off"
									data-role="number"
									placeholder="0000  0000  0000  0000"/>
							<div className="card-first4digits" ref="first4digits">{first4digits}</div>
						</label>
						<label className="card-expire">
							<div className="card-expire-label">
								VALID<br/>
								THRU
							</div>
							<input
								className="card-input card-input_date"
								ref="dateInput"
								onKeyUp={this.onDateKeyup}
								onKeyDown={this.onDateKeydown.bind(this)}
								onKeyPress={this.onDateKeypress}
								type="text"
								tabIndex="2"
								autoComplete="off"
								data-role="cvv"
								placeholder="00/00"/>
						</label>
						<input
							className="card-name card-input card-input_name"
							ref="nameInput"
							onKeyDown={this.onNameType.bind(this)}
							type="text"
							tabIndex="3"
							autoComplete="off"
							data-role="date"
							placeholder="Your Name"/>
						{type ? <PaySystem className="card-paysystem" type={type}/> : ''}
					</div>
					<div className="card-back">
						<div className="card-flip card-flip_back" onClick={this.toggleCard.bind(this)}/>
						<div className="card-magnetline"/>
						<div className="card-cvv">
							<input
								className="card-input card-input_cvv"
								ref="cvvInput"
								onKeyDown={this.onCvvKeydow.bind(this)}
								type="number"
								autoComplete="off"
								data-role="date"
								tabIndex="-1"
								placeholder="000"
								/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
