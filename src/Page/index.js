import React from 'react'
import ReactDOM from 'react-dom'

import Card from '../Card'

export default function Page() {
	return (
		<div className="page-inner">
			<Card />
		</div>
	)
}