import 'whatwg-fetch';
import 'promise/lib/es6-extensions';
import React from 'react';
import ReactDOM from 'react-dom';

import Page from './Page'

ReactDOM.render(
	<Page />,
  document.getElementById('root')
);