import React from 'react';

export default function PaySystem ({ className, type }) {
	let paySystemClassName = ['paysystem']

	if (type) {
		paySystemClassName.push(`${paySystemClassName}_${type}`);
	}

	if (className) {
		paySystemClassName.unshift(className);
	}

	return <div className={paySystemClassName.join(' ')} />
}