import React from 'react';

export default function Loader({ className }) {
	const loaderClassName = [ 'loader' ];

	if (className) {
		loaderClassName.unshift(className);
	}
	return <div className={loaderClassName.join(' ')} />
}